package main

import (
	"bytes"
	"net/smtp"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/google/uuid"
)

func getincidenttext(id int) string {
	var reason string
	switch id {
	case 1:
		reason = "is unreachable!"
	case 2:
		reason = "is reachable again!"
	case 3:
		reason = "responds with unacceptable code!"
	case 4:
		reason = "responds with normal code again!"
	case 5:
		reason = "latency is to high!"
	case 6:
		reason = "latency is normal again!"
	case 9:
		reason = "response is too big!"
	case 10:
		reason = "response is normal size again!"
	case 11:
		reason = "response is too small!"
	case 12:
		reason = "response is normal size again!"
	}
	return reason
}

type IncidentEmail struct {
	TextIncidents []string
	URL           string
	FancyName     string
	SenderAddr    string
	SendTo        string
	MessageID     string
	Date          string
	SubjectLine   string
}

func sendmail(mailhost string, mailport int, auth smtp.Auth, senderaddr string, sendto string, to []string, url string, incidentid []int, rs ResponseSet, oldrs ResponseSet, fancyname string) error {
	const emailtemplate string = "From: {{.SenderAddr}}\r\nTo: {{.SendTo}}\r\nMessage-ID: <{{.MessageID}}>\r\nDate: {{.Date}}\r\nSubject: {{.SubjectLine}}\r\n\r\nAlert regarding {{.FancyName}} ({{.URL}}) has been triggered:\r\n{{range .TextIncidents}}\r\n* {{.}}{{end}}\r\n"

	var IE IncidentEmail
	IE.Date = time.Now().Format(time.RFC1123)
	IE.URL = rs.Name
	IE.FancyName = fancyname
	IE.SubjectLine = "Change for " + fancyname
	IE.SenderAddr = senderaddr
	IE.SendTo = sendto
	for _, incident := range incidentid {
		IE.TextIncidents = append(IE.TextIncidents, getincidenttext(incident))
	}
	messageid, err := uuid.NewUUID()
	if err != nil {
		return err
	}
	splitsenderaddr := strings.Split(senderaddr, "@")
	IE.MessageID = messageid.String() + "@" + splitsenderaddr[1]

	t := template.Must(template.New("email").Parse(emailtemplate))
	buf := new(bytes.Buffer)
	err = t.Execute(buf, IE)
	if err != nil {
		return err
	}

	// Set up authentication information.

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	err = smtp.SendMail(mailhost+":"+strconv.Itoa(mailport), auth, senderaddr, to, []byte(buf.String()))
	if err != nil {
		return err
	}
	return nil
}
