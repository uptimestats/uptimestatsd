package main

import (
	"io/ioutil"
	"log"
)

// LoadConfig loads a json config file and returnes configuration.
func LoadConfig(path string) (*Configuration, error) {
	dat, err := ioutil.ReadFile(path)
	var config *Configuration
	if err = fromjson(string(dat), &config); err != nil {
		log.Fatal(err)
	}
	return config, err
}

type Database struct {
	DatabaseAddress  string
	DatabasePassword string
	DatabaseUser     string
	DatabaseName     string
}

type EmailAuth struct {
	Username            string
	Password            string
	Host                string
	SendingEmailAddress string
	Port                int
}

type Configuration struct {
	EnableQueuemaker   bool
	ListeningAddress   string
	LogFileLocation    string
	TLSKey             string
	TLSCert            string
	Timezone           string
	DB                 Database
	Email              EmailAuth
	DomainGetQueueSize int
}
