package main

import (
	"database/sql"
	"flag"
	"log"
	"net/http"
	"net/smtp"
	"os"
	"time"

	_ "git.pjkramme.de/uptimestats/mysql"
)

const (
	GetAllRequestIntervals string = "SELECT DISTINCT request_interval FROM urls"
	GetAllDomainsString    string = "SELECT name FROM urls;"
)

var (
	db               *sql.DB
	GetAllDomainsSQL *sql.Stmt
	DomainChannel    chan DomainGetResponse
	TimezoneLocation *time.Location
	emailauth        smtp.Auth

	GetDomainByNameSQL *sql.Stmt
	conf               *Configuration
)

func main() {
	var err error
	configfilelocation := flag.String("f", "config.json", "Config File Location")
	flag.Parse()
	conf, err = LoadConfig(*configfilelocation)
	if err != nil {
		log.Fatal(err)
	}
	if conf.LogFileLocation != "" {
		f, err := os.OpenFile(conf.LogFileLocation, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			log.Fatalln(err)
		}
		defer f.Close()
		log.SetOutput(f)
	}
	emailauth = smtp.PlainAuth("", conf.Email.Username, conf.Email.Password, conf.Email.Host)

	if conf.Timezone != "" {
		TimezoneLocation, err = time.LoadLocation(conf.Timezone)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		TimezoneLocation, err = time.LoadLocation("")
		if err != nil {
			log.Fatal(err)
		}
	}

	DomainChannel = make(chan DomainGetResponse, conf.DomainGetQueueSize)
	log.Println("STARTUP connecting to database")

	// Setup database connection
	db, err = sql.Open("mysql", conf.DB.DatabaseUser+":"+conf.DB.DatabasePassword+"@tcp("+conf.DB.DatabaseAddress+")/"+conf.DB.DatabaseName)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("STARTUP connection to database successful")
	log.Println("STARTUP creating database")
	err = dbcreate(db)
	if err != nil {
		log.Fatal(err)
	}

	if conf.EnableQueuemaker {
		log.Println("STARTUP Starting QueueMaker")
		go QueueMaker(db, DomainChannel)
	}
	log.Println("STARTUP starting webserver")

	if conf.EnableQueuemaker {
		http.HandleFunc("/get", DomainGet)
	}
	http.HandleFunc("/post", func(w http.ResponseWriter, r *http.Request) {
		DomainPost(w, r, db)
	})
	srv := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		Addr:         conf.ListeningAddress,
		Handler:      nil,
	}
	log.Fatal(srv.ListenAndServe())
}
