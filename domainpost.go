package main

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type DBResponseSet struct {
	ResponseCode int
	Latency      string
	Timestamp    time.Time
	Size         int
	Reachable    bool
	Proto        string
	Headers      map[string][]string
}

type ResponseSet struct {
	ID            int
	Name          string
	ResponseCode  int
	Latency       string
	LatencyParsed time.Duration
	Timestamp     time.Time
	Size          int
	Reachable     bool
}

type RuleSet struct {
	MinLatency           string
	MaxLatency           string
	MinLatencyParsed     time.Duration
	MaxLatencyParsed     time.Duration
	MinSize              int
	MaxSize              int
	AllowedResponseCodes []int
	Reachable            bool
}

// Handler for Domain Information Post Requests
func DomainPost(w http.ResponseWriter, r *http.Request, db *sql.DB) {
	if r.Method != http.MethodPost {
		http.Error(w, "Only POST allowed", http.StatusMethodNotAllowed)
		return
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	rs, rsencoded, err := ParseResponse(body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Get information about url from the database:
	// * id, name, rules, contacts from urls table
	// * max response, id from responses
	_, fancyname, rules, enablereporting, contacts, oldrsid, oldrs, err := GetURLInformation(rs.ID)
	if err != nil {
		if err != sql.ErrNoRows {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		res, err := db.Exec("INSERT INTO responses(url_id, time, response) VALUES(?, ?, ?)", rs.ID, rs.Timestamp, string(rsencoded))
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		rsid, err := res.LastInsertId()

		_, err = db.Exec("INSERT INTO lastresponse(url_id, response_id) VALUES(?, ?)", rs.ID, rsid)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		return
	}
	// insert into responses
	res, err := db.Exec("INSERT INTO responses(url_id, time, response) VALUES(?, ?, ?)", rs.ID, rs.Timestamp, string(rsencoded))
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	rsid, err := res.LastInsertId()

	_, err = db.Exec("UPDATE lastresponse SET response_id = ? WHERE url_id = ?", rsid, rs.ID)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Evaluate if rules are violated, if they are, insert violation into database with foreign keys to urlid, both responses and the violation codes

	reason := rules.Violated(rs, oldrs)
	if len(reason) > 0 {
		reasonjson, err := json.Marshal(reason)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		_, err = db.Exec("INSERT INTO incidents(url_id, response_id, old_response_id, reason) VALUES(?,?,?,?)", rs.ID, rsid, oldrsid, reasonjson)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if enablereporting {
			err := sendmail(conf.Email.Host, conf.Email.Port, emailauth, conf.Email.SendingEmailAddress, "Monitoring User", contacts, rs.Name, reason, rs, oldrs, fancyname)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

}

func ResponseCodeHit(ResponseCode int, ResponseCodeList []int) bool {
	for _, code := range ResponseCodeList {
		if code == ResponseCode {
			return true
		}
	}
	return false
}

// ParseResponse parses the requestbody into one set for application internal usage and one for db insertion
func ParseResponse(body []byte) (rs ResponseSet, rsfordbstring []byte, err error) {
	err = json.Unmarshal(body, &rs)
	if err != nil {
		return rs, rsfordbstring, err
	}
	rs.LatencyParsed, err = time.ParseDuration(rs.Latency)
	if err != nil {
		return rs, rsfordbstring, err
	}

	var rsfordb DBResponseSet

	// ResponseSet for DB insert
	err = json.Unmarshal(body, &rsfordb)
	if err != nil {
		return rs, rsfordbstring, err
	}
	rsfordbstring, err = json.Marshal(rsfordb)
	if err != nil {
		return rs, rsfordbstring, err
	}
	return rs, rsfordbstring, nil
}

func GetURLInformation(urlid int) (url string, fancyname string, rules RuleSet, enablereporting bool, Contacts []string, oldrsid int, oldrs ResponseSet, err error) {
	var ContactJSON, OldRSJSON, RulesJSON []byte
	err = db.QueryRow("select urls.name, urls.fancyname, urls.rules, urls.Contacts, urls.enablereporting, responses.id, responses.response from lastresponse join urls on lastresponse.url_id = urls.id join responses on lastresponse.response_id = responses.id where lastresponse.url_id = ?;", urlid).Scan(&url, &fancyname, &RulesJSON, &ContactJSON, &enablereporting, &oldrsid, &OldRSJSON)
	if err != nil {
		return
	}
	err = json.Unmarshal(ContactJSON, &Contacts)
	if err != nil {
		return
	}
	oldrs, _, err = ParseResponse(OldRSJSON)
	if err != nil {
		return
	}
	err = json.Unmarshal(RulesJSON, &rules)
	if err != nil {
		return
	}
	rules.MaxLatencyParsed, err = time.ParseDuration(rules.MaxLatency)
	if err != nil {
		return
	}
	rules.MinLatencyParsed, err = time.ParseDuration(rules.MinLatency)
	if err != nil {
		return
	}
	return
}
