package main

const (
	unreachableafterreachable int = iota + 1
	reachableafterunreachable
	errorcodeafternormalcode
	normalcodeaftererrorcode
	toohighlatencyafternormallatency
	normallatencyaftertoohighlatency
	toolowlatencyafternormallatency
	normallatencyaftertoolowlatency
	toobigresponsesizeafternormalresponsesize
	normalresponsesizeaftertoobigresponsesize
	toosmallresponsesizeafternormalresponsesize
	normalresponsesizeaftertoosmallresponsesize
)

func (rule RuleSet) Violated(rs ResponseSet, oldrs ResponseSet) (reasons []int) {
	// URL is reachable...
	if rs.Reachable {
		// ... but was it reachable before?
		if !oldrs.Reachable {
			reasons = append(reasons, reachableafterunreachable)
		}
		newresponsecodehit := ResponseCodeHit(rs.ResponseCode, rule.AllowedResponseCodes)
		oldresponsecodehit := ResponseCodeHit(oldrs.ResponseCode, rule.AllowedResponseCodes)
		//log.Println(rule.AllowedResponseCodes, rs.ResponseCode, newresponsecodehit, oldrs.ResponseCode, oldresponsecodehit)
		if !newresponsecodehit && oldresponsecodehit {
			// URL shows error after being ok
			reasons = append(reasons, errorcodeafternormalcode)
		} else if newresponsecodehit && !oldresponsecodehit && oldrs.ResponseCode != 0 {
			// URL shows ok after having error (with errorcode != 0 and != AllowedResponseCodes)
			// When it was down, the error code was 0. But when it wasn't confirmed down, there is no point in sending an "ok"
			reasons = append(reasons, normalcodeaftererrorcode)
		}

		if rs.LatencyParsed.Nanoseconds() > rule.MaxLatencyParsed.Nanoseconds() && oldrs.LatencyParsed.Nanoseconds() <= rule.MaxLatencyParsed.Nanoseconds() {
			// Latency too high after being ok
			reasons = append(reasons, toohighlatencyafternormallatency)
		} else if rs.LatencyParsed.Nanoseconds() <= rule.MaxLatencyParsed.Nanoseconds() && oldrs.LatencyParsed.Nanoseconds() > rule.MaxLatencyParsed.Nanoseconds() {
			// Latency back to normal after being to high
			reasons = append(reasons, normallatencyaftertoohighlatency)
		}

		if rs.Size > rule.MaxSize && oldrs.Size <= rule.MaxSize {
			reasons = append(reasons, toobigresponsesizeafternormalresponsesize)
		} else if rs.Size <= rule.MaxSize && oldrs.Size > rule.MaxSize {
			reasons = append(reasons, normalresponsesizeaftertoobigresponsesize)
		}

		if rs.Size < rule.MinSize && oldrs.Size >= rule.MinSize {
			reasons = append(reasons, toosmallresponsesizeafternormalresponsesize)
		} else if rs.Size >= rule.MinSize && oldrs.Size < rule.MinSize {
			reasons = append(reasons, normalresponsesizeaftertoosmallresponsesize)
		}

		// toolowlatencyafternormallatency not implemented
		// normallatencyaftertoolowlatency not implemented
	} else if !rs.Reachable && oldrs.Reachable {
		reasons = append(reasons, unreachableafterreachable)
	}
	return
}
