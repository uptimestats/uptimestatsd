package main

import "database/sql"

const (
	DatabaseSetupURLsTable string = `
CREATE TABLE IF NOT EXISTS urls (
    id INT NOT NULL AUTO_INCREMENT,
    name varchar(1024) NOT NULL,
    fancyname varchar(1024) NOT NULL,
    request_interval varchar(32) NOT NULL,
	rules JSON NOT NULL,
	contacts JSON NOT NULL,
    enable BOOLEAN NOT NULL,
    enablereporting BOOLEAN NOT NULL,
    PRIMARY KEY (id),
    INDEX(name)
);`

	DatabaseSetupResponsesTable string = `
CREATE TABLE IF NOT EXISTS responses (
    id INT NOT NULL AUTO_INCREMENT,
    url_id INT NOT NULL,
    time TIMESTAMP NOT NULL,
    response JSON NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (url_id) REFERENCES urls(id) ON DELETE CASCADE,
    INDEX(time)
);`

	DatabaseSetupLastResponseTable string = `
CREATE TABLE IF NOT EXISTS lastresponse (
    url_id INT NOT NULL,
    response_id INT NOT NULL,
    PRIMARY KEY (url_id),
    FOREIGN KEY (url_id) REFERENCES urls(id) ON DELETE CASCADE,
    FOREIGN KEY (response_id) REFERENCES responses(id)
);`

	DatabaseSetupIncidentTable string = `
CREATE TABLE IF NOT EXISTS incidents (
    id INT NOT NULL AUTO_INCREMENT,
    url_id INT NOT NULL,
    response_id INT NOT NULL,
    old_response_id INT NOT NULL,
    reason JSON NOT NULL,
    comment TEXT,
    PRIMARY KEY (id),
    FOREIGN KEY (url_id) REFERENCES urls(id) ON DELETE CASCADE,
    FOREIGN KEY (response_id) REFERENCES responses(id),
    FOREIGN KEY (old_response_id) REFERENCES responses(id)
);`
)

func dbcreate(db *sql.DB) error {
	DatabaseSetupURLsTablePrepared, err := db.Prepare(DatabaseSetupURLsTable)
	if err != nil {
		return err
	}
	_, err = DatabaseSetupURLsTablePrepared.Exec()
	if err != nil {
		return err
	}

	DatabaseSetupResponsesTablePrepared, err := db.Prepare(DatabaseSetupResponsesTable)
	if err != nil {
		return err
	}
	_, err = DatabaseSetupResponsesTablePrepared.Exec()
	if err != nil {
		return err
	}

	DatabaseSetupLastResponseTablePrepared, err := db.Prepare(DatabaseSetupLastResponseTable)
	if err != nil {
		return err
	}
	_, err = DatabaseSetupLastResponseTablePrepared.Exec()
	if err != nil {
		return err
	}

	DatabaseSetupIncidentTablePrepared, err := db.Prepare(DatabaseSetupIncidentTable)
	if err != nil {
		return err
	}
	_, err = DatabaseSetupIncidentTablePrepared.Exec()
	if err != nil {
		return err
	}

	return nil
}
