package main

import (
	"database/sql"
	"io"
	"log"
	"net/http"
	"time"
)

func QueueWorker(db *sql.DB, intervalstring string, interval time.Duration, dq chan DomainGetResponse) error {
	rows, err := db.Query("SELECT name, id FROM urls WHERE request_interval = ? AND enable = true", intervalstring)
	if err != nil {
		return err
	}
	for rows.Next() {
		var url string
		var id int
		err := rows.Scan(&url, &id)
		if err != nil {
			return err
		}
		dq <- DomainGetResponse{Url: url, ID: id}
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	rows.Close()
	return nil
}

func QueueHandler(db *sql.DB, intervalstring string, dq chan DomainGetResponse) {
	// ticker with interval "interval"
	// register into "Queues" Map
	// push domains from table into channel
	interval, err := time.ParseDuration(intervalstring)
	if err != nil {
		log.Println(err)
	}

	log.Println("QUEUEHANDLER", interval, "spawned.")
	log.Println("QUEUEHANDLER", interval, "kickstarting queue")
	err = QueueWorker(db, intervalstring, interval, dq)
	if err != nil {
		log.Println("QUEUEHANDLER", interval, err)
	}
	ticker := time.NewTicker(interval)
	go func() {
		for range ticker.C {
			err = QueueWorker(db, intervalstring, interval, dq)
			if err != nil {
				log.Println("QUEUEHANDLER", interval, err)
			}
		}
	}()
}

func QueueMaker(db *sql.DB, dq chan DomainGetResponse) {
	ticker := time.NewTicker(10 * time.Second)
	go func() {
		queuemap := make(map[string]int)
		for range ticker.C {
			rows, err := db.Query("SELECT DISTINCT request_interval from urls")
			if err != nil {
				log.Println(err)
			}
			for rows.Next() {
				var intervalstring string
				err := rows.Scan(&intervalstring)
				if err != nil {
					log.Println(err)
				}
				interval, err := time.ParseDuration(intervalstring)
				if err != nil {
					log.Println(err)
				}
				if _, ok := queuemap[intervalstring]; !ok {
					log.Println("QUEUEMAKER Found new interval", intervalstring)

					log.Println("QUEUEMAKER Spawning thread for interval", interval)
					go QueueHandler(db, intervalstring, dq)
					// just set something so we now we already spawned that
					queuemap[intervalstring] = 1
				}
			}
			err = rows.Err()
			if err != nil {
				log.Fatal(err)
			}
			rows.Close()
		}
	}()
	// Check how many different intervals there are
	// Initialize timers, channels and arrays for the urls (maps?) for all different intervals with cache time
	// spawn handlers for all different timers, when the timer is finished, everything should be bursted into the channel
	return
}

type DomainGetResponse struct {
	Url string `json:"url"`
	ID  int    `json:"id"`
}

func DomainGet(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Only GET allowed", http.StatusMethodNotAllowed)
		return
	}
	if len(DomainChannel) == 0 {
		http.Error(w, "No domains in queue right now.", http.StatusNoContent)
		return
	}

	jsonresponse, err := toprettyjson(<-DomainChannel)
	if err != nil {
		log.Println("DOMAINGET", err)
		http.Error(w, "Internal JSON Parsing Error. Please report.", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, string(jsonresponse))
}
